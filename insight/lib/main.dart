import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart';
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';
import 'dart:convert' as convert;
import 'package:flutter_session/flutter_session.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';


import 'dart:convert';

import 'ClientDashboard.dart';
import 'dart:io';


String os = Platform.operatingSystem;
String user, jina, etoken=" ";
ProgressDialog pr;
String role="0";


FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
const mainColor = Color(0xfff1e83c5);
const nyekundu = Color(0xffe82224);
const tabcolor = Color(0xffe1dede);
const listcolor = Color(0xffffbfbfd);
const kijani = Colors.green;
const gray = Color(0xffa9a9a9);

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  SharedPreferences prefs = await SharedPreferences.getInstance();
  jina = prefs.getString('user_id');
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitDown,
    DeviceOrientation.portraitUp,
  ]);
  WidgetsFlutterBinding.ensureInitialized();
  await SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
  OneSignal.shared.setLogLevel(OSLogLevel.verbose, OSLogLevel.none);

  OneSignal.shared.init(
      "30a54049-8b9b-40e8-b921-af51440943ad",

      iOSSettings: {
        OSiOSSettings.autoPrompt: false,
        OSiOSSettings.inAppLaunchUrl: false
      }
  );
  OneSignal.shared.setInFocusDisplayType(OSNotificationDisplayType.notification); 

  runApp(MyApp());

}

class MyApp extends StatelessWidget {
  Future<bool> isLoggedIn() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    jina = prefs.getString('user_id');
    role= prefs.getString('isSFP');
    print(jina);
    if(jina!=null)
      return true;
    else
      return false;
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: FutureBuilder(
          future: isLoggedIn(),
          builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
            if (snapshot.hasData) {
              return snapshot.data ? (ClientDashboard()) : LoginPage() ;
            }
            return Container(); // noop, this builder is called again when the future completes
          },
        )
    );
  }
}


class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();

}

class _LoginPageState extends State<LoginPage> {
  bool _obscureText = true;
  String email, password;
  String username;
  String userId;
  int companyId;
  String companyName;
  List<Null> emergencyContacts;
  String latestMessage;
  String iconUrl;
  bool isAuthenticated;
  String authMessage;
  final _text = TextEditingController();
  bool _validate = false;
  final _formKey = GlobalKey<FormState>();
  final _formKeyy = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    firebaseCloudMessaging_Listeners();

  }


  Widget _buildLogo() {
    return Center(

      child:  Container(
          height: 80.0,
          width: 250.0,
          child: Padding(
              padding: EdgeInsets.all(1),
              child: CircleAvatar(
                backgroundColor: Colors.transparent,
                radius: 10,
                child: new Image.asset('assets/gicon.png'),
              )),
          decoration: new BoxDecoration(
            color:Color(0xffffbfbfd),
            shape: BoxShape.rectangle,
            border: new Border.all(
              color:mainColor,
              width: 0.3,
            ),
          ))
    );


  }

  Future<void> _doSignIn() async {
    print(email);
    print(password);
    print(os);
    print(etoken);
    pr.show();
    String apiUrl = "http://survey.geeckoltd.com/mobile/V2/userlogin";
    Map<String, String> headers = {"Content-type": "application/json"};
    final json =  convert.jsonEncode({"email": email, "password": password,"DeviceType": os,"DeviceToken": etoken});
    http.Response response = await http.post(apiUrl,headers: headers, body: json);
    String jsonsDataString = response.body.toString();

    if (response.statusCode == 200) {
      pr.hide().then((isHidden) {
        print(isHidden);
      });



      Map<String, dynamic> json = jsonDecode(jsonsDataString);

      Map<String, dynamic> map=(json['basic_details']);
      print("idd ni "+jsonsDataString);

      print("idd ni "+map.toString());
      if(map==null)
      {
        Fluttertoast.showToast(
            msg: "Incorrect Username Or Password",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            timeInSecForIos: 3,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );
      }


      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString("user_id",map['user_id'].toString());
      prefs.setString("token", map['token'].toString());
      prefs.setString("name",map['name'].toString());
      prefs.setString("telephone",map['telephone'].toString());
      prefs.setString("avatar",map['avatar'].toString());
      prefs.setString("email",map['email'].toString());
      prefs.setString("postal_address",map['postal_address'].toString());
      prefs.setString("Department",map['Department'].toString());
      prefs.setString("street_address",map['street_address'].toString());
      prefs.setString("Dob",map['Dob'].toString());
      prefs.setString("gender",map['gender'].toString());
      prefs.setString("HomeCounty",map['HomeCounty'].toString());
      prefs.setString("ip_id",map['ip_id'].toString());
      prefs.setString("CompanyLogoPath",map['CompanyLogoPath'].toString());
      prefs.setString("Nationality",map['Nationality'].toString());
      prefs.setString("ContactId",map['ContactId'].toString());
      prefs.setString("isActive",map['isActive'].toString());


        Navigator.push(context, new MaterialPageRoute(
            builder: (context) => new ClientDashboard())
        );
    }
    else {
      pr.hide().then((isHidden) {
        print(isHidden);
      });
      Fluttertoast.showToast(
          msg: "Incorrect Username Or Password",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIos: 3,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0
      );
      print("nooo");
    }
  }


  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context,type: ProgressDialogType.Normal, isDismissible: true, showLogs: true);
    pr.style(
        message: 'Logging in...',
        borderRadius: 10.0,
        backgroundColor: Colors.grey.withOpacity(0.8),
        progressWidget: CircularProgressIndicator(),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600)
    );
    return Container(
      child: Scaffold(
        backgroundColor: Color(0xfff1e83c5),
        body: SingleChildScrollView(
          child: Stack(
          children: <Widget>[

            Column(

              children: <Widget>[
                Container(height: MediaQuery.of(context).size.height * 0.1, color: mainColor),
                _buildContainer(),

              ],
            )
          ],
        ),
    ),),
    );
  }
  Widget _buildEmailRow() {
    return Padding(
      key: _formKey,
      padding: EdgeInsets.only(left: 30, right: 30),
      child: Container(
        child: TextFormField(
          onChanged: (value) {
            setState(() {
             email = value;
            });
          },
        decoration: InputDecoration(
          prefixIcon: Icon(
            Icons.person,
            color: mainColor,
          ),
          contentPadding: EdgeInsets.all(0),
          enabledBorder: new OutlineInputBorder(
            borderRadius: new BorderRadius.circular(15.0),
            borderSide:  BorderSide(color: mainColor ),

          ),
          focusedBorder: new OutlineInputBorder(
            borderRadius: new BorderRadius.circular(15.0),
            borderSide:  BorderSide(color: mainColor ),

          ),
          labelText: "Email",
        ),
      ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: Colors.white,
        ),
    ),
    );
  }

  Widget _buildPasswordRow() {
    return Padding(
      key: _formKeyy,
      padding: EdgeInsets.only(left: 30, right: 30),
      child: Container(
        child: TextFormField(
          obscureText: true,
      onChanged: (value) {
          setState(() {
            password = value;
          });
      },
          decoration: InputDecoration(
            prefixIcon: Icon(
              Icons.lock,
              color: mainColor,
            ),
            contentPadding: EdgeInsets.all(0),
            enabledBorder: new OutlineInputBorder(
              borderRadius: new BorderRadius.circular(15.0),
              borderSide:  BorderSide(color: mainColor ),

            ),
            focusedBorder: new OutlineInputBorder(
              borderRadius: new BorderRadius.circular(15.0),
              borderSide:  BorderSide(color: mainColor ),

            ),
            labelText: "Password",
          ),
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: Colors.white,
        ),
      ),
    );
  }

  Widget _buildForgetPasswordButton() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        FlatButton(
          onPressed: () {},
          child: Text("Forgot Password"),
        ),
      ],
    );
  }

  Widget _buildLoginButton() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Container(
          height: 1.4 * (MediaQuery.of(context).size.height / 25),
          width: 6.5 * (MediaQuery.of(context).size.width / 10),
          margin: EdgeInsets.only(bottom: 20),
          child: RaisedButton(
            elevation: 5.0,
            color: mainColor,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15.0),
            ),
            onPressed: () {
              _doSignIn();
            },
            child: Text(
              "Log In",
              style: TextStyle(
                color: Colors.white,
                fontSize: MediaQuery.of(context).size.height / 40,
              ),
            ),
          ),
        )
      ],
    );
  }
  Widget _buildContainer() {

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        ClipRRect(
          borderRadius: BorderRadius.only(
            topRight: Radius.circular(40.0),
            topLeft: Radius.circular(40.0),
            bottomLeft: Radius.circular(40.0),
            bottomRight: Radius.circular(40.0),
          ),
          child: Container(
            height: MediaQuery.of(context).size.height * 0.83,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              color: listcolor,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[

                Container(height: 20, color: Colors.transparent),

                _buildLogo(),

                Container(height: 80, color: Colors.transparent),

                _buildEmailRow(),
                Container(height: 35, color: Colors.transparent),
                _buildPasswordRow(),
                Container(height: 50, color: Colors.transparent),
                _buildLoginButton(),
              ],
            ),
          ),
        ),
      ],
    );
  }

  void firebaseCloudMessaging_Listeners() {
    if (Platform.isIOS) iOS_Permission();

    _firebaseMessaging.getToken().then((token){
      etoken=token;
      print("token ni "+token);
      print("os ni "+os);

    });

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print('on message $message');
      },
      onResume: (Map<String, dynamic> message) async {
        print('on resume $message');
      },
      onLaunch: (Map<String, dynamic> message) async {
        print('on launch $message');
      },
    );
  }

  void iOS_Permission() {
    _firebaseMessaging.requestNotificationPermissions(
        IosNotificationSettings(sound: true, badge: true, alert: true)
    );
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings)
    {
      print("Settings registered: $settings");
    });
  }
}



