import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart';
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';
import 'dart:convert' as convert;
import 'package:flutter_session/flutter_session.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter_open_whatsapp/flutter_open_whatsapp.dart';


import 'ProfilePage.dart';


const mainColor = Color(0xfff1e83c5);
const nyekundu = Color(0xffe32e16);
const tabcolor = Color(0xffe1dede);
String jina="  ", kampuni="  ";
SharedPreferences prefs;
String _platformVersion = 'Unknown';


class InkWellDrawer extends StatefulWidget {
  _InkWellDrawer createState() => _InkWellDrawer();

}

class  _InkWellDrawer extends State< InkWellDrawer> {

  @override
  void initState() {
    super.initState();
    _restore();
    initPlatformState();
  }
  Future<void> initPlatformState() async {
    String platformVersion;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      platformVersion = await FlutterOpenWhatsapp.platformVersion;
    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }

    if (!mounted) return;

    setState(() {
      _platformVersion = platformVersion;
    });
  }
  _restore() async {
    print('restoring...');

    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      jina = prefs.getString('name');
      kampuni = prefs.getString('email').toString();
    });


  }
  Widget _createHeader() {
    return SizedBox(
      height : 235.0,
      child  : new DrawerHeader(
        margin: EdgeInsets.zero,
        padding: EdgeInsets.all(20),
        decoration: BoxDecoration(
          color: mainColor,
        ),

        child: Stack(

          children: <Widget>[
            Align(
              alignment: Alignment.centerLeft + Alignment(.4, -.7),
              child:  Container(

                  child: Padding(
                      padding: EdgeInsets.all(5),
                      child: CircleAvatar(
                        radius: 55,
                        child: new Image.asset('assets/icon.png'),
                        backgroundColor: Color(0xffffbfbfd),
                      )),
                  decoration: new BoxDecoration(
                    color: Colors.transparent,
                    shape: BoxShape.circle,
                    border: new Border.all(
                      color:mainColor,
                      width: 2.0,
                    ),
                  )),
            ),
            Align(
              alignment: Alignment.bottomLeft+ Alignment(.3, -.2),
              child: Text(
                jina,
                style: TextStyle(color: Colors.white, fontSize: 20.0),
              ),
            ),
            Align(
              alignment: Alignment.bottomLeft + Alignment(.3, .1),
              child: Text(
                kampuni,
                style: TextStyle(
                  color: Colors.white70,
                ),
              ),
            ),
          ],
        ),
      ),
    );

  }
  @override
  Widget build (BuildContext ctxt) {

    return new Drawer(
      child: Container(
        color: tabcolor,
      child: ListView(
        children: <Widget>[
          _createHeader(),
          CustomListTile(Icons.person, 'Profile', ()=>{
            Navigator.pop(ctxt),
            Navigator.push(ctxt,
                new MaterialPageRoute(builder: (ctxt) => new ProfilePage())
            )
          }),
          CustomListTile(Icons.note_sharp, 'Terms and Privacy', ()=>{
            _launchPolicy()
          }),
          CustomListTile(Icons.bug_report, 'Report an Issue', ()=>{
            _launchBug()
          }),
          CustomListTile(Icons.star, 'Rate This App', ()=>{
            _launchRate()
          }),
          Spacer(),
          Container(height: 50, color: Colors.transparent),

          Center(
            child: Text('Version 1.1'),

          ),
        ],

      ),
      ),
    );
  }
  _launchPolicy() async {
    const url = 'http://alerts.syve.co.ke/termandconditions';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
  _launchRate() async {
    const url = 'https://play.google.com/store/apps/details?id=com.geecko.mne';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
  _launchCaller(String number) async {
    String url = "tel:$number";
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
  _launchBug() async {

    await launch(
        "https://wa.me/${254732897628}?text=Hello...");
  }
  _launchContact() async {

    await launch(
        "https://wa.me/${254795749348}?text=Hello...");
  }
}


class CustomListTile extends StatelessWidget{

  final IconData icon;
  final  String text;
  final Function onTap;

  CustomListTile(this.icon, this.text, this.onTap);
  @override
  Widget build(BuildContext context){
    //ToDO
    return Padding(
      padding: const EdgeInsets.fromLTRB(8.0, 0, 8.0, 0),
      child:Container(
        decoration: BoxDecoration(
            border: Border(bottom: BorderSide(color: Colors.grey.shade400))
        ),
        child: InkWell(
            splashColor: Colors.orangeAccent,
            onTap: onTap,
            child: Container(
                height: 40,
                child: Row(
                  mainAxisAlignment : MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(children: <Widget>[
                      Icon(icon),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                      ),
                      Text(text, style: TextStyle(
                          fontSize: 16
                      ),),
                    ],),
                    Icon(Icons.arrow_right)
                  ],)
            )
        ),
      ),
    );
  }
}