import 'dart:async';

import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';
import 'dart:convert' as convert;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:geolocator/geolocator.dart';
import 'package:geocoder/geocoder.dart';
import 'package:intl/intl.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';


import 'InkWellDrawer.dart';

const mainColor = Color(0xfff1e83c5);
const nyekundu = Color(0xffe32e16);
const tabcolor = Color(0xffe1dede);
const nyeupe = Color(0xffffffff);
const listcolor = Color(0xffffbfbfd);
const kijani = Colors.green;
const gray = Color(0xffa9a9a9);
const nyeusi= Color(0xff000000);

String cdst="Fetching Location... ";
String jina=" ";
String address="Loading address... ";
String id="";
String lat=" ";
String lon=" ";
String alt=" ";
String town=" ";
String akishoni=" ";
String street=" ";
String fon=" ";
String emair=" ";
String nem=" ";
String lastCheck=" ";
ProgressDialog pr, prr;
LatLng _center;
String messo=" ";
String refreshh="0";
String mesho="No updates found in the last four hours";
String  company;
String usertokeni = '';
String clogo="http://alerts.p-count.org/k.png";
String alogo="http://alerts.p-count.org/dirLogos/rock_logo.PNG";

GoogleMapController controller;
int hesabu=0;
String defaultMessage = "Enchogu";
Set<Marker> markers;
LatLng currentLocation =
LatLng(-1.286389, 36.817223);
Marker m;
FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
String etokeni=" ";
List<ActiveAlerts> activeAlerts;
FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
new FlutterLocalNotificationsPlugin();

class ClientDashboard extends StatefulWidget {

  _ClientDashboard createState() => _ClientDashboard();

}


class  _ClientDashboard extends State< ClientDashboard> {



  @override
  void initState() {
    super.initState();
    _restore();
    _determinePosition();
    initPlatformState();


  }


  _restore() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    jina = prefs.getString('name');
    usertokeni = prefs.getString('token');
    nem= prefs.getString('SFPName');
    fon= prefs.getString('SFPTelephone');
    emair= prefs.getString('SFPEmailAddress');
   // clogo= prefs.getString('CompanyLogo');
    alogo= prefs.getString('AppLogo');
    id= prefs.getString('user_id');

    print("Aye enchogu, id ni "+id);
    setState(() {
      setState(() {
        OneSignal.shared.setExternalUserId(id);
        jina;
        lastCheck;
        usertokeni;
        nem;
        emair;
        fon;
      });
    });
  }
  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    StreamSubscription<Position> positionStream = Geolocator.getPositionStream(desiredAccuracy: LocationAccuracy.best, timeInterval: 30000).listen(

            (position) async {
          print(DateTime.now());
          final coordinates = new Coordinates(position.latitude, position.longitude);
          setState(()
          {
            position;
            print("Sasa Antho");

            cdst= "("+position.latitude.toString() +", "+position.longitude.toString()+")"+position.altitude.toStringAsFixed(4);
            print(cdst);
            lat=position.latitude.toString();
            lon=position.longitude.toString();

          });
          setState(() async {
            var addresses = await Geocoder.local.findAddressesFromCoordinates(coordinates);
            var first = addresses.first;
            address=first.locality+", "+first.countryName;
            town=first.locality;
            street=first.addressLine;
          });

        });

    if (!serviceEnabled) {
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.deniedForever) {
      return Future.error(
          'Location permissions are permantly denied, we cannot request permissions.');
    }

    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission != LocationPermission.whileInUse &&
          permission != LocationPermission.always) {
        return Future.error(
            'Location permissions are denied (actual value: $permission).');
      }
    }

    return await Geolocator.getCurrentPosition();
  }
  Future<void> _alert() async {

    if(akishoni=="Acknowledge")
    {
      lon=" ";
      lat=" ";
    }
    pr.show();
    print("sasa");
    String apiUrl = "http://sms.geeckoltd.com/mobile/V1/AcknowledgeAlert";
    Map<String, String> headers = {"Content-type": "application/json","Accept": "application/json","token": usertokeni };
    final json =  convert.jsonEncode({"RecNo": id, "Latitude": lat, "Longitude": lon});
    http.Response response = await http.post(apiUrl,headers: headers, body: json);
    String jsonsDataString = response.body.toString();

    print("Endmonth");
    print(id);

    print(jsonsDataString);
    if (response.statusCode == 200) {
      pr.hide().then((isHidden) {
        print(isHidden);
      });

      String jsonsDataString = response.body.toString();

      print("Endmonth");

      print(jsonsDataString);
      pr.hide().then((isHidden) {
        print(isHidden);
      });
      setState(() { refreshh; });
      Fluttertoast.showToast(
          msg: "Reporting succesful!",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIos: 3,
          backgroundColor: Colors.white,
          textColor: Colors.black,
          fontSize: 16.0
      );

    }
    else {
      pr.hide().then((isHidden) {
        print(isHidden);
      });
      print("no");
    }
  }

  Future<List<ActiveAlerts>> _getUsers() async {

    String apiUrl = "http://survey.geeckoltd.com/mobile/V2/getSurveys";
    Map<String, String> headers = {"Content-type": "application/json","token": usertokeni };
    http.Response response = await http.get(apiUrl,headers: headers);
    if (response.statusCode == 200) {

      String jsonsDataString = response.body.toString();

      Map<String, dynamic> map = jsonDecode(jsonsDataString);

      List<ActiveAlerts> alerts = [];

      if (map['surveyList'] != null) {
        print("sawasawa anto");
        activeAlerts = new List<ActiveAlerts>();
        map['surveyList'].forEach((v) {


          ActiveAlerts alert = ActiveAlerts(v["Id"].toString(),v["SurveyCode"].toString(),v["SurveyEXcept"].toString(), v["SurveyName"].toString(), v["AccessLink"].toString());

          alerts.add(alert);
          print("200 bob imepotea");
        });
        print(alerts.length);

        return alerts;

      }




    }
    else {
      print("no");
    }

  }


  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context,type: ProgressDialogType.Normal, isDismissible: true, showLogs: true);
    pr.style(
        message: 'Reporting...',
        borderRadius: 10.0,
        backgroundColor: Colors.grey.withOpacity(0.8),
        progressWidget: CircularProgressIndicator(),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600)
    );
    DateTime now = DateTime.now();
    String formattedDate = DateFormat('hh:mm a').format(now);
    setState(() {
      formattedDate;
    });
    return Scaffold(
        drawer: InkWellDrawer(),
      backgroundColor: listcolor,

      appBar: AppBar(
        backgroundColor: mainColor,

          title:
        Row(
          mainAxisAlignment: MainAxisAlignment.center ,//Center Row contents horizontally,
          crossAxisAlignment: CrossAxisAlignment.center, //Center Row contents vertically,
          children: <Widget>[
            Expanded(
              child: Container(
                margin: const EdgeInsets.only(bottom: 25.0, top: 30.0, right:25),
                child: Image.asset('assets/survey.png'),
              ),
            ),

          ],
        ),
           centerTitle: true,
      ),
        body: SingleChildScrollView(
          child: Stack(

            children: <Widget>[

              Column(
                crossAxisAlignment: CrossAxisAlignment.center,

                children: <Widget>[
                  Container(
                    height: MediaQuery.of(context).size.height * 0.9,
                    color: mainColor,
                    child: FutureBuilder(
                      future: _getUsers(),
                      builder: (BuildContext context, AsyncSnapshot snapshot){
                        if(snapshot.data == null){
                          return Container(
                              color:Colors.white,
                              child: Center(
                                  child: CircularProgressIndicator()
                              )
                          );
                        } else {
                          return
                            Container(
                              color: nyeupe,
                              child: ListView.builder(
                                padding: EdgeInsets.only(bottom:300, top:5),
                                itemCount: snapshot.data.length,
                                itemBuilder: (BuildContext context, int index) {
                                  return Container(
                                    color: nyeupe,
                                    child: Card(
                                      color: tabcolor,
                                      margin: EdgeInsets.all(1.9),
                                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                                      elevation: 4.0,
                                      child: Container(
                                          decoration: BoxDecoration(color: tabcolor,
                                            borderRadius: BorderRadius.circular(0),
                                          ),
                                          child: ListTile(
                                            contentPadding: EdgeInsets.symmetric(horizontal: 5.0),


                                            title: Container(
                                              padding: EdgeInsets.only(right: 13.0,bottom: 6, left:13.0),


                                              child: Text(
                                                snapshot.data[index].SurveyCode+" : "+snapshot.data[index].SurveyEXcept,
                                                style: TextStyle(
                                                    color:nyeusi,
                                                    fontFamily: 'Montserrat-Regular',
                                                    fontWeight:FontWeight.bold,

                                                ),
                                              ),

                                            ),

                                            // subtitle: Text("Intermediate", style: TextStyle(color: Colors.white)),
                                            subtitle:
                                            Container(
                                              padding: EdgeInsets.only(right: 13.0, left:13.0),


                                              child: Text(
                                                snapshot.data[index].SurveyName,
                                                style: TextStyle(
                                                    color:nyeusi,
                                                    fontFamily: 'Montserrat-Regular'

                                                ),
                                              ),

                                            ),
                                        trailing:
                                        Icon(Icons.keyboard_arrow_right, color: mainColor, size: 30.0),
                                            onTap: () {
                                              showDialog(
                                                  context: context,
                                                  builder: (context) {
                                                    return Card(
                                                        child: WebviewScaffold(
                                                          url: snapshot.data[index].AccessLink,
                                                          appBar: AppBar(
                                                            title: Text(snapshot.data[index].SurveyEXcept),
                                                            centerTitle: true,
                                                          ),
                                                          withZoom: true,
                                                          withLocalStorage: true,
                                                          initialChild: Container(
                                                            color: Colors.white,
                                                            child: const Center(
                                                              child: CircularProgressIndicator(),
                                                            ),
                                                          ),
                                                        )
                                                    );
                                                  }
                                              );
                                            }

                                          )
                                      ),
                                    ),
                                  );
                                },
                              ),
                            );
                        }
                      },
                    ),


                  ),


                ],
              ),
            ],

          ),
        ),


    );
  }

  _textMe(String number) async {
    // Android
    String uri = "sms:$number";
    if (await canLaunch(uri)) {
      await launch(uri);
    } else {
      // iOS
      String uri = "sms:$number";
      if (await canLaunch(uri)) {
        await launch(uri);
      } else {
        throw 'Could not launch $uri';
      }
    }
  }

  _launchCaller(String number) async {
    String url = "tel:$number";
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}



class ActiveAlerts {
  String Id;
  String SurveyCode;
  String SurveyEXcept;
  String SurveyName;
  String AccessLink;


  ActiveAlerts(this.Id, this.SurveyCode, this.SurveyEXcept, this.SurveyName,  this.AccessLink);

}

Future<void> initPlatformState() async {


  print("Before set log");
  //OneSignal.shared.setLogLevel(OSLogLevel.verbose, OSLogLevel.none);
  print("Before set user privacy");
  //OneSignal.shared.setRequiresUserPrivacyConsent(_requireConsent);
  OneSignal.shared.setExternalUserId(id);

  var settings = {
    OSiOSSettings.autoPrompt: false,
    OSiOSSettings.promptBeforeOpeningPushUrl: true
  };

  OneSignal.shared.setNotificationReceivedHandler((notification) {


  });

  OneSignal.shared
      .setNotificationOpenedHandler((OSNotificationOpenedResult result) {
    print("Payload only is"+result.notification.payload.jsonRepresentation().toString());

  });
  OneSignal.shared.setExternalUserId(id);

  OneSignal.shared
      .setSubscriptionObserver((OSSubscriptionStateChanges changes) {
    print("SUBSCRIPTION STATE CHANGED: ${changes.jsonRepresentation()}");
  });

  OneSignal.shared.setPermissionObserver((OSPermissionStateChanges changes) {
    print("PERMISSION STATE CHANGED: ${changes.jsonRepresentation()}");
  });
  OneSignal.shared.setExternalUserId(id);


  // NOTE: Replace with your own app ID from https://www.onesignal.com
  await OneSignal.shared
      .init("30a54049-8b9b-40e8-b921-af51440943ad", iOSSettings: settings);

  OneSignal.shared.consentGranted(true);
  OneSignal.shared
      .setInFocusDisplayType(OSNotificationDisplayType.notification);
  await OneSignal.shared.getPermissionSubscriptionState();
  bool requiresConsent = await OneSignal.shared.requiresUserPrivacyConsent();

}